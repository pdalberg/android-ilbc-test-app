package com.googlecode.androidilbc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder.AudioSource;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Demo extends Activity {
	
	public static final String TAG = "ilbc";
	
	private AudioTrack mTrack;
	
	private Button mButton, btnPlay, btnStop;

	private Recorder mRecorder;
	private Player mPlayer;

	public static final int STATE_IDLE = 70;

	public static final int STATE_RECORDING = 71;

	public static final int STATE_RECORDED = 72;

	public static final int STATE_PLAYING = 73;

	private int mState;
	
	
	class Player {

		public Player() {
			int bufferSize;
			bufferSize = AudioTrack.getMinBufferSize(8000,
					AudioFormat.CHANNEL_CONFIGURATION_MONO,
					AudioFormat.ENCODING_PCM_16BIT);
			mTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 8000,
					AudioFormat.CHANNEL_CONFIGURATION_MONO,
					AudioFormat.ENCODING_PCM_16BIT, bufferSize,
					AudioTrack.MODE_STREAM);
		}


		
		class PlayThread extends Thread {
			
			@Override
			public void run(){
				mPlayer.play(Environment.getExternalStorageDirectory().getPath() + "/iLBC_30ms.BIT");
				//mPlayer.play(Environment.getExternalStorageDirectory().getPath() + "/rec.ilbc");
			}
			
		}

		public void play(String path) {
			
//			byte[] samples = new byte[4096 * 10];
			byte[] data = new byte[152000];

//			int samplesLength;
//			int dataLength;
			
			File file = new File(path);

			int audioLength = (int) (file.length());
			 
					
					//new byte[audioLength];
			try {
				
				byte[] audio = IOUtil.readFile(file);

				
				String strPlaystate = "";
				switch(mTrack.getPlayState()){
					case AudioTrack.STATE_UNINITIALIZED:
						strPlaystate = "AudioTrack.STATE_UNINITIALIZED";
						break;
					case AudioTrack.PLAYSTATE_PAUSED:
						strPlaystate = "AudioTrack.PLAYSTATE_PAUSED";
						break;
					case AudioTrack.PLAYSTATE_PLAYING:
						strPlaystate = "AudioTrack.PLAYSTATE_PLAYING";
						break;
					case AudioTrack.PLAYSTATE_STOPPED:
						strPlaystate = "AudioTrack.PLAYSTATE_STOPPED";
						break;
				}
				
				
				
				Log.i(TAG, "playstate = " + strPlaystate);				
				
				Log.i(TAG, "audio length = " + audio.length + " bytes");
				
				Codec.instance().decode(audio, 0, audio.length, data, 0);

				mTrack.write(data, 0, data.length);
			
				mTrack.play();


				

			} catch (Exception e) {
				Log.e(TAG, "play(path) " + e.getMessage());
			}
		}
		
		public void startPlaying(){
			new PlayThread().start();
		}
		
		public void stopPlaying(){
			mTrack.stop();
		}
	}

	

	class Recorder {

		private String mPath;

		private int mBufferSize;

		private AudioRecord mRecorder;

		private FileOutputStream mOut;

		class RecordThread extends Thread {
			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {

				while (true) {

					if (null != mRecorder
							&& AudioRecord.RECORDSTATE_RECORDING == mRecorder
									.getRecordingState()) {

						final byte[] samples;
						final byte[] data;
						final int bytesRecord;
						final int bytesEncoded;

						samples = new byte[mBufferSize];
						// TODO: calculate data size.
						data = new byte[mBufferSize];

						bytesRecord = mRecorder.read(samples, 0, mBufferSize);

						if (bytesRecord == AudioRecord.ERROR_INVALID_OPERATION) {
							Log.e(TAG,
									"read() returned AudioRecord.ERROR_INVALID_OPERATION");
						} else if (bytesRecord == AudioRecord.ERROR_BAD_VALUE) {
							Log.e(TAG,
									"read() returned AudioRecord.ERROR_BAD_VALUE");
						} else if (bytesRecord == AudioRecord.ERROR_INVALID_OPERATION) {
							Log.e(TAG,
									"read() returned AudioRecord.ERROR_INVALID_OPERATION");
						}

						bytesEncoded = Codec.instance().encode(samples, 0,
								bytesRecord, data, 0);

						try {
							mOut.write(data, 0, bytesEncoded);
						} catch (IOException e) {
							Log.e(TAG, "Failed to write");
						}
						
					} else {
						break;
					}
				}

			}
		}

		public void setSavePath(String path) throws IOException {
			File file = new File(path);

			if (file.exists()) {
				Log.i(TAG, "Remove exists file, " + path);
			}

			file.createNewFile();

			if (!file.canWrite()) {
				throw new IOException("Cannot write to " + path);
			}

			mPath = path;

		}

		public void start() throws IOException {

			int truncated;

			mBufferSize = AudioRecord.getMinBufferSize(8000,
					AudioFormat.CHANNEL_CONFIGURATION_MONO,
					AudioFormat.ENCODING_PCM_16BIT);

			if (mBufferSize == AudioRecord.ERROR_BAD_VALUE) {
				Log.e(TAG, "buffer error");
				return;
			}

			// 480 bytes for 30ms(y mode)
			truncated = mBufferSize % 480;
			if (truncated != 0) {
				mBufferSize += 480 - truncated;
				Log.i(TAG, "Extend buffer to " + mBufferSize);
			}

			try {
				mOut = new FileOutputStream(new File(mPath));

				// Write File Header
				mOut.write("#!iLBC30\n".getBytes());

			} catch (FileNotFoundException e) {

				throw new IOException("File not found");
			}

			mRecorder = new AudioRecord(AudioSource.MIC, 8000,
					AudioFormat.CHANNEL_CONFIGURATION_MONO,
					AudioFormat.ENCODING_PCM_16BIT, mBufferSize);

			mRecorder.startRecording();

			new RecordThread().start();
		}

		public void stop() {

			if (null == mRecorder) {

				Log.w(TAG, "Recorder has not start yet");
				return;
			}
			mRecorder.stop();
			try {
				mOut.close();
			} catch (IOException e) {
			}
		}

		private void record() {

			int bufferSize;
			bufferSize = AudioRecord.getMinBufferSize(8000,
					AudioFormat.CHANNEL_CONFIGURATION_MONO,
					AudioFormat.ENCODING_PCM_16BIT);

			if (bufferSize == AudioRecord.ERROR_BAD_VALUE) {
				Log.e(TAG, "buffer error");
				return;
			}

			bufferSize *= 10;

			AudioRecord record = new AudioRecord(AudioSource.MIC, 8000,
					AudioFormat.CHANNEL_CONFIGURATION_MONO,
					AudioFormat.ENCODING_PCM_16BIT, bufferSize);

			Log.d(TAG, "buffer size: " + bufferSize);
			byte[] tempBuffer = new byte[bufferSize];
			record.startRecording();

			int bufferRead = record.read(tempBuffer, 0, bufferSize);
			if (bufferRead == AudioRecord.ERROR_INVALID_OPERATION) {
				throw new IllegalStateException(
						"read() returned AudioRecord.ERROR_INVALID_OPERATION");
			} else if (bufferRead == AudioRecord.ERROR_BAD_VALUE) {
				throw new IllegalStateException(
						"read() returned AudioRecord.ERROR_BAD_VALUE");
			} else if (bufferRead == AudioRecord.ERROR_INVALID_OPERATION) {
				throw new IllegalStateException(
						"read() returned AudioRecord.ERROR_INVALID_OPERATION");
			}
			// Close resources…
			record.stop();
			Log.e(TAG, "playing, " + bufferRead);

			byte[] samples = new byte[4096 * 10];
			byte[] data = new byte[4096 * 10];

			int samplesLength;
			int dataLength;

			dataLength = Codec.instance().encode(tempBuffer, 0, bufferRead,
					data, 0);

			Log.d(TAG, "encode " + bufferRead + " to " + dataLength);

			Log.d(TAG, "data[0]: " + data[0] + "data[dataLength-1]: "
					+ data[dataLength - 1]);

			samplesLength = Codec.instance().decode(data, 0, dataLength,
					samples, 0);

			Log.d(TAG, "decode " + dataLength + " to " + samplesLength);

			Log.d(TAG, "samples[0]: " + samples[0]
					+ "samples[samplesLength-1]: " + samples[samplesLength - 1]);
			//new Player().play(samples, 0, samplesLength);

			// new Player().play(tempBuffer, 0, bufferRead);
			Log.e(TAG, "rec");

		}

	}

	

	// / Called when the activity is first created. /
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mRecorder = new Recorder();

		mPlayer = new Player();

		mButton = (Button) findViewById(R.id.button);
		mButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Log.i(TAG, "mState = " + mState);
				switch (mState) {

				case STATE_IDLE:
					updateState(STATE_RECORDING);
					break;
				case STATE_RECORDING:
					updateState(STATE_RECORDED);
					break;
				case STATE_RECORDED:
					updateState(STATE_PLAYING);
					break;
				default:
					break;
				}

			}
		});
		updateState(STATE_IDLE);
		
		btnPlay = (Button)findViewById(R.id.btnPlay);
		btnPlay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mPlayer.startPlaying();
				
			}
		});
		
		btnStop = (Button)findViewById(R.id.btnStop);
		btnStop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mPlayer.stopPlaying();
				
			}
		});

	}

	private void updateState(int state) {

		mState = state;
		switch (state) {
		case STATE_IDLE:
			mButton.setText("Record");
			break;

		case STATE_RECORDING:
			mButton.setText("Recording");
			try {
				mRecorder.setSavePath(Environment.getExternalStorageDirectory().getPath() + "/rec.ilbc");
				mRecorder.start();
			} catch (IOException e) {
				Log.e(TAG, " " + e.getMessage());
			}

			break;
		case STATE_RECORDED:
			mRecorder.stop();
			mButton.setText("Play");
			break;
		case STATE_PLAYING:
			Log.i(TAG, "playing...");
			mButton.setText("Record");
			mPlayer.startPlaying();
			break;
		}
	}
}